# **Getir Node.js Rest API**
---
This project containing RESTful API service with Node.js, Express, MongoDB and Jest.

## **Dependencies**

 Used in projects : **[nodejs](https://nodejs.org/en/)**
 
---

## **Node.js Version**
Stable : 16.14.0

You can run it in the stable version for successful build.

---

## **Project Setup**

To perform the installation, it will be sufficient to run the following command on the terminal screen while in the project directory.

    npm install

---

## **Terminal Commands**

---

    npm run server

Run the project with nodemon to keep it development with hot reload. Project will be started on localhost.

---

    npm run start

Run the project with node without any changes listen. Project will be started on localhost.

---

    npm run test

 **[jest](https://jestjs.io/)** used as a test framework in this project.

---

## **DEMO**

**[Heroku Link](https://burak-duman-getir.herokuapp.com/)**

POST Request: "https://burak-duman-getir.herokuapp.com/api/records"

Sample:
```
{
"startDate": "2016-01-26",
"endDate": "2018-02-02",
"minCount": 2700,
"maxCount": 3000
}
```


---
## Can't run? 

##### Please follow these quick steps

1. Make sure you have connected to Internet.
2. Make sure you have installed requirements.
3. Make sure you have using stable Node.js version
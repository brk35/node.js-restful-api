const app = require('../app');

const _PORT = process.env.PORT || 5000;

app.listen(_PORT, () => console.log(`Server at running on ${_PORT}`))

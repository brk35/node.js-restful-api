const express = require('express');
const initDBConnection = require('./loaders/db')
const cors = require('cors');

//Routes
const records = require('./routes');

const app = express();

// BodyParser and Cors Middleware
app.use(express.json());
app.use(cors());

//Init MongoDB
initDBConnection();

//Routes defining
app.use('/', records);

module.exports = app;

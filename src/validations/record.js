const { body } = require('express-validator')


/**
 * Record model validations for REST API
 * @returns {Array}
 */
module.exports = () => {
  return [
    body('startDate').exists().isDate(),
    body('endDate').exists().isDate().custom((value, { req }) => {
      if (new Date(req.body.startDate) > new Date(value)) {
        return Promise.reject('endDate must be greater than startDate');
      }
      return true
    }),
    body('minCount').exists().isInt().custom(value => {
      if (value <= 0) {
        return Promise.reject('minCount must be greater than or equal to 0');
      }
      return true
    }),
    body('maxCount').exists().isInt().custom((value, { req }) => {
      if (value < req.body.minCount) {
        return Promise.reject('maxCount must be greater than minCount');
      }
      return true
    })
  ]
}

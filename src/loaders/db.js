const mongoose = require('mongoose');
const { MONGO_URI } = require('../config');

const initDBConnection = async () => {
  // Connection initialization of MongoDB
  await mongoose.connect(MONGO_URI)
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.log('MongoDB Error: ', err))

  // Initial DB connection error log
  mongoose.connection.on('error', (err) => {
    console.log('Current DB connection error: ', err);
  });
};

module.exports = initDBConnection;

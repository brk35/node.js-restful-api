const express = require('express');
const apiRouter = express.Router();
const recordValidation = require('../../validations/record');
const validationMiddleware = require('../../middlewares/validation');
const { filter } = require('../../services/record');

/**
 * Get filtered records from data collection
 * @routes POST api/records
 */
apiRouter.post('/records', recordValidation(), validationMiddleware, filter)

//Handle other methods which is not defined
apiRouter.get('/records', (req, res) => { res.sendStatus(404) });
apiRouter.put('/records', (req, res) => { res.sendStatus(404) });
apiRouter.delete('/records', (req, res) => { res.sendStatus(404) });

module.exports = apiRouter;

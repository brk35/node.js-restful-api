const express = require('express');
const swaggerUi = require('swagger-ui-express');
const recordsRoutes = require('./api/records');
var swaggerDocument = require('../documents/swagger.json')

const router = express.Router();

router.use('/api', recordsRoutes);

//Swagger documentation implement
router.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//Export all defined routes
module.exports = router;

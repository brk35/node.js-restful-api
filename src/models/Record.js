const mongoose = require('mongoose');
const { Schema } = require('mongoose');

/**
 * Model defining of record
 */
const RecordSchema = new Schema({
  key: {
    type: String,
  },
  createdAt: {
    type: Date
  },
  counts: {
    type: [Number]
  }
})

const model = mongoose.model('Record', RecordSchema);

module.exports = model;
const serverError = (res, err) => {
  res.status(500).json({
    code: 2,
    msg: 'Server Error',
    error: err
  });
}

const sendSuccess = (res, data) => {
  res.status(200).json({
    code: 0,
    msg: 'Success',
    records: data
  });
}

module.exports = {
  serverError,
  sendSuccess
}
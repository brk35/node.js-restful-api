const mongoose = require('mongoose');
const request = require('supertest')
const app = require('../../app')


describe('Record Post Endpoint', () => {

  afterAll(() => {
    // Closing the DB connection allows Jest to exit successfully.
    mongoose.connection.close();
  })


  it('should get filtered records', async () => {
    const res = await request(app)
      .post('/api/records')
      .send({
        startDate: '2016-01-26',
        endDate: '2018-02-02',
        minCount: 2700,
        maxCount: 3000
      })
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('code', 0)
  })

  it('should get error for invalid request', async () => {
    const res = await request(app)
      .post('/api/records')
      .send({
        startDate: '2016-01-26',
        endDate: '2018-02-02',
        minCount: 2700,
        maxCount: 1000
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body).toHaveProperty('errors')
  })
})
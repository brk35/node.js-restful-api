const mongoose = require('mongoose');
const request = require('supertest')
const app = require('../../app')


describe('Record Routes Test', () => {

  afterAll(() => {
    // Closing the DB connection allows Jest to exit successfully.
    mongoose.connection.close();
  })


  it('should not response for get request', async () => {
    const res = await request(app)
      .get('/api/records')
    expect(res.statusCode).toEqual(404)
  })

  it('should not response for put request', async () => {
    const res = await request(app)
      .put('/api/records')
    expect(res.statusCode).toEqual(404)
  })

  it('should not response for delete request', async () => {
    const res = await request(app)
      .delete('/api/records')
    expect(res.statusCode).toEqual(404)
  })

})
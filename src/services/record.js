/**
 * Record Model
 */
const Record = require('../models/Record');

const { serverError, sendSuccess } = require('../utils/response/responseHandler');
const filter = async (req, res) => {

  try {
    const { startDate, endDate, minCount, maxCount } = req.body;

    const records = await Record.aggregate(
      [
        {
          // Filter by start and end date
          $match: {
            createdAt: {
              $gte: new Date(startDate),
              $lte: new Date(endDate)
            }
          }
        },
        {
          // Total value calculation of counts
          $addFields: {
            totalCount: {
              $reduce: {
                input: '$counts',
                initialValue: 0,
                in: { $add: ['$$value', '$$this'] }
              }
            }
          },
        },
        {
          // Filter by start and end date
          $match: {
            totalCount: {
              $gte: minCount,
              $lte: maxCount
            }
          }
        },
        {
          // Removing unnecesseray parameters
          $unset: ['_id', 'value', 'counts']
        }
      ]
    )

    // 200 Response Handler
    sendSuccess(res, records);
  } catch (error) {
    // 500 Error Handle
    serverError(res, error);
  }

}

module.exports = {
  filter
}
const { validationResult } = require('express-validator');

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 * @param {NextFunction} next 
 * @returns {Object}
 */
module.exports = (req, res, next) => {
  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return res.status(400).json(
      {
        code: 1,
        msg: 'Required parameters is not right!',
        errors: errors.array()
      }
    );
  }

  next();
}